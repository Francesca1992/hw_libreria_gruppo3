package com.lezione23.hw_libreria_gruppo3;

public class Categoria {

    private int categoriaID;
    private String nomeCategoria;

    Categoria(){

    }

    public int getCategoriaID() {
        return categoriaID;
    }
    public void setCategoriaID(int categoriaID) {
        this.categoriaID = categoriaID;
    }
    public String getNomeCategoria() {
        return nomeCategoria;
    }
    public void setNomeCategoria(String nomeCategoria) {
        this.nomeCategoria = nomeCategoria;
    }

}
