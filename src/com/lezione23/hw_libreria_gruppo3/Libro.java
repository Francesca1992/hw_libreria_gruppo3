package com.lezione23.hw_libreria_gruppo3;

import java.util.ArrayList;

public class Libro {

	private int libroID;
	private String titolo;
	private String codice;
	private ArrayList<Autore> autori;
	private ArrayList<Categoria> categorie;

	Libro() {

	}

	public int getLibroID() {
		return libroID;
	}

	public void setLibroID(int libroID) {
		this.libroID = libroID;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public ArrayList<Autore> getAutori() {
		return autori;
	}

	public void setAutori(ArrayList<Autore> autori) {
		this.autori = autori;
	}

	public ArrayList<Categoria> getCategorie() {
		return categorie;
	}

	public void setCategorie(ArrayList<Categoria> categorie) {
		this.categorie = categorie;
	}

	public String stampaLibro() {
		return "Libro [libroID=" + libroID + ", titolo=" + titolo + ", codice=" + codice + ", autori=" + autori + ", categorie=" + categorie + "]";
	}

}