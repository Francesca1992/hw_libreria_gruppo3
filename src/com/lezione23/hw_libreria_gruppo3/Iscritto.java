package com.lezione23.hw_libreria_gruppo3;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Iscritto {

    private int iscrittoID;
    private String nomeUtente;
    private String cognomeUtente;
    private String codFis;
    private String tessera;
    private ArrayList<Prestito> prestiti;

    Iscritto(){

    }
    Iscritto(String varNome, String varCognome, String var){

    }

    public int getIscrittoID() {
        return iscrittoID;
    }
    public void setIscrittoID(int iscrittoID) {
        this.iscrittoID = iscrittoID;
    }
    public String getNomeUtente() {
        return nomeUtente;
    }
    public void setNomeUtente(String nomeUtente) {
        this.nomeUtente = nomeUtente;
    }
    public String getCognomeUtente() {
        return cognomeUtente;
    }
    public void setCognomeUtente(String cognomeUtente) {
        this.cognomeUtente = cognomeUtente;
    }
    public String getCodFis() {
        return codFis;
    }
    public void setCodFis(String codFis) {
        this.codFis = codFis;
    }
    public String getTessera() {
        return tessera;
    }
    public void setTessera(String tessera) {
        this.tessera = tessera;
    }
    public void setTessera() {
        this.tessera = this.getNomeUtente().substring(0,2) + this.getCognomeUtente().substring(0,2) + "-" + stringaData();
    }
    public ArrayList<Prestito> getPrestiti() {
        return prestiti;
    }
    public void setPrestiti(ArrayList<Prestito> prestiti) {
        this.prestiti = prestiti;
    }

    private String stringaData() {
        LocalDateTime varData = LocalDateTime.now();

        String gg = String.format("%02d", varData.getDayOfMonth());
        String MM = String.format("%02d", varData.getMonthValue());
        String aaaa = String.format("%04d", varData.getYear());
        String hh = String.format("%02d", varData.getHour());
        String mm = String.format("%02d", varData.getMinute());
        String ss = String.format("%02d", varData.getSecond());

        return gg + MM + aaaa.substring(2) + hh + mm + ss;
    }
}
