package com.lezione23.hw_libreria_gruppo3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GestioneAutori {
	
	private ArrayList<Autore> elencoAutori = new ArrayList<Autore>();

	// SELECT
	public void selectAutori() {

		elencoAutori.clear();
		
		try {
			
			Connection conn = ConnettoreDB.getIstanza().getConnessione();

			String querySelect = "SELECT autoreID, nomeAutore, cognomeAutore FROM Autore";
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(querySelect);
			ResultSet risultato = ps.executeQuery();
			
			while (risultato.next()) {
				Autore autTemp = new Autore();
				autTemp.setAutoreID(risultato.getInt(1));
				autTemp.setNomeAutore(risultato.getString(2));
				autTemp.setCognomeAutore(risultato.getString(3));

				this.elencoAutori.add(autTemp);
			}
			
			this.stampaAutoriInConsole();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void stampaAutoriInConsole() {
			
			for (int i = 0; i < this.elencoAutori.size(); i++) {
	
				Autore temp = this.elencoAutori.get(i);
				System.out.println(temp.stampaAutore());
			}
		}
	
	//INSERT
	public void insertAutori(String varNomeAutore, String varCognomeAutore) {
		
		try {
			
			Connection conn = ConnettoreDB.getIstanza().getConnessione();
			
			String queryInsert = "INSERT INTO Autore(nomeAutore, cognomeAutore) VALUES (?, ?)";
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(queryInsert);
			ps.setString(1, varNomeAutore);
			ps.setString(2, varCognomeAutore);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
