package com.lezione23.hw_libreria_gruppo3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GestioneIscritti {
    private ArrayList<Iscritto> elencoIscritti = new ArrayList<Iscritto>();

    //SELECT
    public void selectIscritti() {
        elencoIscritti.clear();

        try {
            Connection conn = ConnettoreDB.getIstanza().getConnessione();
            String querySelect = "SELECT iscrittoID, nomeUtente, cognomeUtente, codFis, tessera FROM Iscritto";

            PreparedStatement ps = (PreparedStatement)conn.prepareStatement(querySelect);
            ResultSet risultato = ps.executeQuery();
            while (risultato.next()) {
                Iscritto iscTemp = new Iscritto();
                iscTemp.setIscrittoID(risultato.getInt(1));
                iscTemp.setNomeUtente(risultato.getString(2));
                iscTemp.setCognomeUtente(risultato.getString(3));
                iscTemp.setCodFis(risultato.getString(4));
                iscTemp.setTessera(risultato.getString(5));

                this.elencoIscritti.add(iscTemp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //INSERT
    public void insertIscritti(String varNome, String varCognome, String varCodFis) {
        try {
            Connection conn = ConnettoreDB.getIstanza().getConnessione();
            String queryInsert = "INSERT INTO Iscritto(nomeUtente, cognomeUtente, codFis) VALUES (?, ?, ?)";

            PreparedStatement ps = (PreparedStatement)conn.prepareStatement(queryInsert);
            ps.setString(1, varNome);
            ps.setString(2, varCognome);
            ps.setString(3, varCodFis);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
