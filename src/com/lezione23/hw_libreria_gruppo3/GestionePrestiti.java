package com.lezione23.hw_libreria_gruppo3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class GestionePrestiti {

    private ArrayList<Prestito> elenco_prestiti = new ArrayList<Prestito>();

    // SELECT
    public void selectPrestiti() {

        try {

            Connection conn = ConnettoreDB.getIstanza().getConnessione();

            String querySelect = "SELECT prestitoID, numPrestito, dataAcc, dataRest, iscritto_rif FROM Prestito";
            PreparedStatement ps = (PreparedStatement) conn.prepareStatement(querySelect);

            ResultSet rs;
            rs = ps.executeQuery();

            while (rs.next()) {
                Prestito presTemp = new Prestito();
                presTemp.setPrestitoID(rs.getInt(1));
                presTemp.setNumPrestito();
                presTemp.setDataAcc(rs.getString(3));
                presTemp.setDataRest(rs.getString(4));
                presTemp.setIscritto_rif(rs.getInt(5));
                
                this.elenco_prestiti.add(presTemp);
            }
            
            this.stampaPrestitiInConsole();

        } catch (SQLException e) {
            System.out.println("ERRORE DI CONNESSIONE");
            e.printStackTrace();
        }
    }
    
    //STAMPA LIBRO IN CONSOLE 
    private void stampaPrestitiInConsole() {
    	for(int i=0; i<this.elenco_prestiti.size(); i++) {
    	Prestito temp = this.elenco_prestiti.get(i);
    	System.out.println(temp.stampaPrestito());
    	}
    	}

    // INSERT
    public boolean inserisciPrestiti(String varDataAcc, String varDataRest, int varIscrittoRif) {
        Prestito prest = new Prestito();
        prest.setNumPrestito();
        prest.setDataAcc(varDataAcc);
        prest.setDataRest(varDataRest);
        prest.setIscritto_rif(varIscrittoRif);

        try {
            insert(prest);

            if(prest.getPrestitoID() > 0)
                return true;

            return false;

        } catch (SQLException e) {
            return false;
        }


//      throw new SQLException();   //ATTENZIONE: Comunque termino la funzione con una Exception anche se il ramo ELSE della IF non esiste
    }

    private Prestito insert(Prestito prest) throws SQLException {

        Connection connessione = ConnettoreDB.getIstanza().getConnessione();
        String query_inserimento = "INSERT INTO Prestito(numPrestito, dataAcc, dataRest, iscritto_rif) VALUE (?, ?, ?)";
        PreparedStatement ps = (PreparedStatement)connessione.prepareStatement(query_inserimento, Statement.RETURN_GENERATED_KEYS);

        ps.setString(1, prest.getNumPrestito());
        ps.setString(2, prest.getDataAcc());
        ps.setString(3, prest.getDataRest());
        ps.setInt(4, prest.getIscritto_rif());

        ps.executeUpdate();
        
        ResultSet risultato = ps.getGeneratedKeys();
        risultato.next();

        int id_risultante = risultato.getInt(1);
        prest.setPrestitoID(id_risultante);

        return prest;
    }
}