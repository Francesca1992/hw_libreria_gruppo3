package com.lezione23.hw_libreria_gruppo3;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Prestito {

    private int prestitoID = 0;
    private String numPrestito;
    private String dataAcc;
    private String dataRest;
    private int iscritto_rif;

    Prestito() {

    }

    Prestito(String numPrestito, String dataAcc, String dataRest, int varIscrittoRif) {
        this.numPrestito = numPrestito;
        this.dataAcc = dataAcc;
        this.dataRest = dataRest;
        this.iscritto_rif = varIscrittoRif;
    }

    public int getIscritto_rif() {
		return iscritto_rif;
	}

	public void setIscritto_rif(int iscritto_rif) {
		this.iscritto_rif = iscritto_rif;
	}

	public int getPrestitoID() {
        return prestitoID;
    }

    public void setPrestitoID(int prestitoID) {
        this.prestitoID = prestitoID;
    }

    public String getNumPrestito() {
        return numPrestito;
    }

    public void setNumPrestito() {
        LocalDateTime giornoOraCorrente = LocalDateTime.now();
        DateTimeFormatter formattazione_ita = DateTimeFormatter.ofPattern("ddMMyyyyHHmmss");
        String data_formato_ita = giornoOraCorrente.format(formattazione_ita);
        this.numPrestito = data_formato_ita;
    }

    public String getDataAcc() {
        return dataAcc;
    }

    public void setDataAcc(String dataAcc) {
        this.dataAcc = dataAcc;
    }

    public String getDataRest() {
        return dataRest;
    }

    public void setDataRest(String dataRest) {
        this.dataRest = dataRest;
    }

	
	public String stampaPrestito() {
		return "Prestito [prestitoID=" + prestitoID + ", numPrestito=" + numPrestito + ", dataAcc=" + dataAcc
				+ ", dataRest=" + dataRest + ", iscritto_rif=" + iscritto_rif + "]";
	}


}
