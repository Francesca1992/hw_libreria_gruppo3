package com.lezione23.hw_libreria_gruppo3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GestioneLibri {

	private ArrayList<Libro> elencoLibri = new ArrayList<Libro>();

	// SELECT
	public void selectLibri() {

		elencoLibri.clear();
		
		try {
			
			Connection conn = ConnettoreDB.getIstanza().getConnessione();

			String querySelect = "SELECT libroID, titolo, codice FROM Libro";
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(querySelect);
			ResultSet risultato = ps.executeQuery();
			
			while (risultato.next()) {
				Libro libTemp = new Libro();
				libTemp.setLibroID(risultato.getInt(1));
				libTemp.setTitolo(risultato.getString(2));
				libTemp.setCodice(risultato.getString(3));

				this.elencoLibri.add(libTemp);
			}
			
			this.stampaLibriInConsole();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void stampaLibriInConsole() {
		
		for (int i = 0; i < this.elencoLibri.size(); i++) {

			Libro temp = this.elencoLibri.get(i);
			System.out.println(temp.stampaLibro());
		}
	}
	
	//INSERT
	public void insertLibri(String varTitolo, String varCodice) {
		
		try {
			
			Connection conn = ConnettoreDB.getIstanza().getConnessione();
			
			String queryInsert = "INSERT INTO Libro(titolo, codice) VALUES (?, ?)";
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(queryInsert);
			ps.setString(1, varTitolo);
			ps.setString(2, varCodice);
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
