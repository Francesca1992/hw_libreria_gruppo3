package com.lezione23.hw_libreria_gruppo3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GestioneCategorie {
    private ArrayList<Categoria> elencoCategorie = new ArrayList<Categoria>();

    //SELECT
    public void selectCategorie() {
        elencoCategorie.clear();

        try {
            Connection conn = ConnettoreDB.getIstanza().getConnessione();
            String querySelect = "SELECT categoriaID, nomeCategoria FROM Categoria";

            PreparedStatement ps = (PreparedStatement)conn.prepareStatement(querySelect);
            ResultSet risultato = ps.executeQuery();
            while (risultato.next()) {
                Categoria catTemp = new Categoria();
                catTemp.setCategoriaID(risultato.getInt(1));
                catTemp.setNomeCategoria(risultato.getString(2));

                this.elencoCategorie.add(catTemp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //INSERT
    public void insertCategorie(String varCategoria) {
        try {
            Connection conn = ConnettoreDB.getIstanza().getConnessione();
            String queryInsert = "INSERT INTO Categoria(nomeCategoria) VALUE (?)";

            PreparedStatement ps = (PreparedStatement)conn.prepareStatement(queryInsert);
            ps.setString(1, varCategoria);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
		}
	}
}
