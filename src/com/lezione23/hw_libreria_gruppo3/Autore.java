package com.lezione23.hw_libreria_gruppo3;

public class Autore {
	
	private int autoreID;
	private String nomeAutore;
	private String cognomeAutore;
	
	Autore() {
		
	}
	
	public int getAutoreID() {
		return autoreID;
	}
	
	public void setAutoreID(int autoreID) {
		this.autoreID = autoreID;
	}
	
	public String getNomeAutore() {
		return nomeAutore;
	}
	
	public void setNomeAutore(String nomeAutore) {
		this.nomeAutore = nomeAutore;
	}
	
	public String getCognomeAutore() {
		return cognomeAutore;
	}
	
	public void setCognomeAutore(String cognomeAutore) {
		this.cognomeAutore = cognomeAutore;
	}

	public String stampaAutore() {
		return "Autore [autoreID=" + autoreID + ", nomeAutore=" + nomeAutore + ", cognomeAutore=" + cognomeAutore + "]";
	}
	
}